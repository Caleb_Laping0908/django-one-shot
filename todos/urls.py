from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_items,
    create_todo_list,
    edit_todo_list,
    delete_todo_list,
    create_todo_item,
    edit_todo_item,
)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", todo_list_items, name="todo_list_items"),
    path("create/", create_todo_list, name="create_todo_list"),
    path("<int:id>/edit/", edit_todo_list, name="edit_todo_list"),
    path("<int:id>/delete/", delete_todo_list, name="delete_todo_list"),
    path("create_item/", create_todo_item, name="create_todo_item"),
    path("items/<int:id>/edit/", edit_todo_item, name="edit_todo_item"),
]
