from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/lists.html", context)


def todo_list_items(request, id):
    item_list = get_object_or_404(TodoList, id=id)
    context = {
        "item_list": item_list,
    }
    return render(request, "todos/speclist.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_items", id=new_list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def edit_todo_list(request, id):
    item_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=item_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_items", id=id)
    else:
        form = TodoForm(instance=item_list)

    context = {
        "item_list": item_list,
        "form": form,
    }

    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    item_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        item_list.delete()
        return redirect("todo_list_list")

    context = {
        "item_list": item_list,
    }

    return render(request, "todos/delete.html", context)


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_form = form.save()
            return redirect("todo_list_items", id=new_form.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }

    return render(request, "todos/createitem.html", context)


def edit_todo_item(request, id):
    task = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("todo_list_items", id=task.list.id)
    else:
        form = TodoItemForm(instance=task)
    context = {
        "form": form,
    }

    return render(request, "todos/edititem.html", context)
